<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use HasFactory , SoftDeletes;

    protected $fillable = [
        'todo',
        'image',
    ];

    public function Categories ()
    {
        return $this->belongsToMany(Category::class);
    }
}
