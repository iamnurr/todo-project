<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Models\Category;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['todos'] = Todo::with('Categories')->get();
        return view('todo.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = Category::pluck('name','id');
        return view('todo.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        $path = "";
        if($request->hasFile('image')){
            $fileName = Str::snake($request->get('todo')).'.'.$request->file('image')->getClientOriginalExtension();
            $path = Storage::putFileAs('image/todos',$request->file('image'), $fileName);

        }
        $todo = Todo::create([
            'todo' => $request->get('todo'),
            'image' => $path,
        ]);

        if(empty($todo)){
            return redirect()->back()->withInput();
        }

        $todo->categories()->attach($request->get('categories'));

        return redirect()
        ->route('todos.index')
        ->with('SUCCESS', __('Todo has been created.'));
    }

    /**
     * Complete the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function complete(Todo $todo)
    {
        $todo->forceFill([
            'is_complete' => true,
        ]);
        if($todo->update()){
        return redirect()->route('todos.index')->with('SUCCESS', __('Task done.'));
        }
        return redirect()->back()->withInput()->with('ERROR', __('Fail to complete.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        $data['todo'] = $todo;
        $data['categories'] = Category::pluck('name','id');
        return view('todo.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, Todo $todo)
    {
        $todo->todo = $request->get('todo');

        if($request->hasFile('image')){
            if($todo->image){
                Storage::delete($todo->image);
            }
            $fileName = Str::snake($request->get('todo')).'.'.$request->file('image')->getClientOriginalExtension();
            $todo->image = Storage::putFileAs('image/todos',$request->file('image'), $fileName);
        }

        if($todo->update()){
            $todo->categories()->sync($request->get('categories'));
            return redirect()->route('todos.index')->with('SUCCESS', __('Todo has been Updated.'));
        }
        return redirect()->back()->withInput()->with('ERROR', __('Fail to Updated.'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        if($todo->image){
            Storage::delete($todo->image);
        }
        if($todo->delete()){
            $todo->categories()->detach();
            return redirect()->back()->with('SUCCESS', __('Todo has been deleted.'));
        }
        return redirect()->back()->with('ERROR', __('Please try again.'));


    }
}
