<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Todo;

class HomeController extends Controller
{
    public function __invoke ()
    {
        $data['todos'] = Todo::with('Categories')
        ->where('is_complete', true)
        ->get();

        $data['categories'] = Category::all();
        return view('welcome', $data);
    }
}
