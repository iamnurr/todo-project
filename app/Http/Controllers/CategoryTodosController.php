<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryTodosController extends Controller
{
    public function __invoke(Category $category)
    {
        $category->load('Todos.Categories');
        $data['category'] = $category;
        $data['categories'] = Category::all();

        return view('category.todos', $data);
    }
}
