<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categories' => ['required'
            ],
            'todo' => [
                'required',
                Rule::unique('todos')->ignore($this->route('todo')) ,
                'min:5',
                'max:255',
            ],
            'image' => [
                'image:jpg,jpeg,png',
                'max:512',
            ],
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Please select category name.',
            'todo.min' => 'Minimum 5 letter.',
            'image.image' => 'Must be jpg or jpeg or png',
            'image.max' => 'Image maximum size is 512kb.',
        ];
    }
}
