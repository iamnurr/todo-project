<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryTodosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', HomeController::class)->name('home');
Route::get('category/{slug}/todos', CategoryTodosController::class)->name('category.todos');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function () {
    // todos
    Route::resource('todos', TodoController::class)->except('show');
    Route::put('todos/{todo}/complete', [TodoController::class, 'complete'])->name('todos.complete');

    //categories
    Route::resource('categories', CategoryController::class)->except('show');
});

require __DIR__.'/auth.php';
