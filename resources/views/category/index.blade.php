<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Category List') }}
            </h2>
            <a class="px-2 py-1 font-semibold leading-tight text-blue-700 bg-blue-100 rounded-sm" href="{{ route('categories.create')}}">Create Category</a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="w-full">
                        <thead>
                          <tr class="text-md font-semibold tracking-wide text-left text-gray-900 bg-gray-100 uppercase border-b border-gray-600">
                            <th class="px-4 py-3">ID</th>
                            <th class="px-4 py-3">Category</th>
                            <th class="px-4 py-3">Action</th>
                          </tr>
                        </thead>
                        <tbody class="bg-white">
                          @forelse ($categories as $category)
                          <tr class="text-gray-700">
                            <td class="px-4 py-3 border">{{ $category->id}}</td>
                            <td class="px-4 py-3 text-ms font-semibold border">{{$category->name}}</td>
                            <td class="py-3 px-4 border">
                                <a class="px-2 py-1 font-semibold leading-tight text-yellow-700 bg-yellow-100 rounded-sm" href="{{ route('categories.edit', $category->id)}}">Edit</a>
                                <a href="{{route('categories.destroy', $category->id)}}" class="delete-row px-2 py-1 font-semibold leading-tight text-red-700 bg-red-100 rounded-sm" data-confirm="Are You Sure To Delete This?"> Delete </a>

                            </td>
                          </tr>
                          @empty
                          <tr>
                            <td colspan="4" class="px-4 py-3 text-center">{{ __("No Category found.")}}</td>
                        </tr>
                          @endforelse
                        </tbody>
                      </table>
                      <div class="my-4">
                           {{-- {{$categories->links()}} --}}
                      </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
