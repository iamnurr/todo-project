<div class="mb-4">
    <label class="block" for="category">Category</label>
    <select name="categories[]" id="category_id" multiple="true">
        <option value="">{{ __("Select Category")}}</option>
        @foreach ($categories as $id => $name)
            <option @isset($todo)
                        {{$todo->Categories->firstWhere("name",$name) ? "selected=true" : ""}}
                    @endisset value="{{ $id }}" >
                {{ $name }}
            </option>
        @endforeach
    </select>
    <p class=" text-red-500">{{$errors->first("categories")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="task">Task</label>
    <input class="focus" type="text" name="todo" value="{{ old('todo', isset($todo) ? $todo->todo : '')}}" id="todo">
    <p class=" text-red-500">{{$errors->first("todo")}}</p>
</div>
<div class="mb-4">
    <label class="block" for="task">Image</label>
    <input class="focus" type="file" name="image" id="image">
    <p class=" text-red-500">{{$errors->first("image")}}</p>
</div>
<div>
    <a class="p-1 pl-3 pr-3 bg-transparent border-2 border-gray-500 text-gray-500 text-lg rounded-lg hover:bg-gray-500 hover:text-gray-100 focus:border-4 focus:border-gray-300" href="{{ route('todos.index')}}">Back</a>

    <button class="p-1 pl-7 pr-7 bg-transparent border-2 border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300" type="submit">{{$buttonText}}</button>
</div>
